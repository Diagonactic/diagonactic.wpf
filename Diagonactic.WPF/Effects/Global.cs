﻿using System;
using System.Reflection;

namespace Diagonactic.WPF.Effects
{
    /// <summary>
    /// </summary>
    internal static class Global
    {
        private static string _assemblyShortName;

        /// <summary>
        ///     Gets the short name of the assembly.
        /// </summary>
        /// <value>
        ///     The short name of the assembly.
        /// </value>
        private static string AssemblyShortName
        {
            get
            {
                if (_assemblyShortName != null) return _assemblyShortName;
                Assembly a = typeof (Global).Assembly;

                return _assemblyShortName = a.ToString().Split(',')[0];
            }
        }

        /// <summary>
        ///     Helper method for generating a "pack://" URI for a given relative file based on the
        ///     assembly that this class is in.
        /// </summary>
        public static Uri MakePackUri(string relativeFile)
        {
            string uriString = "pack://application:,,,/" + Global.AssemblyShortName + ";component/" + relativeFile;
            return new Uri(uriString);
        }
    }
}