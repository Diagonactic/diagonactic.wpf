/*
Copyright 2015 Matthew S. Dippel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#define LAYERS        (1)
// Placing the light on the bottom gives a chisel effect, on the top gives a bevel effect
// however, simply  changing the size to a negative number also gives a bevel effect.
#define PLACE_LIGHT_ON_BOTTOM false

sampler2D input : register(s0);
float4 DdxDdy : register(c9);

/// <summary>Size (in pixels)</summary>
/// <type>Double</type>
/// <minValue>0</minValue>
/// <maxValue>5</maxValue>
/// <defaultValue>0.5</defaultValue>
float Size : register (c1);
/// <summary>Shadow</summary>
/// <type>Double</type>
/// <minValue>0</minValue>
/// <maxValue>5</maxValue>
/// <defaultValue>2.0</defaultValue>
float ShadowIntensity : register (c2);
/// <summary>Glow</summary>
/// <type>Double</type>
/// <minValue>0</minValue>
/// <maxValue>5</maxValue>
/// <defaultValue>2.5</defaultValue>
float GlowIntensity : register(c3);
/// <summary>Shadow</summary>
/// <type>Double</type>
/// <minValue>0</minValue>
/// <maxValue>5</maxValue>
/// <defaultValue>3.5</defaultValue>
float MixDivisor : register(c4);

inline float4 getPixel(float2 uv, float yDistance)
{
    uv.y = uv.y + yDistance;
    return tex2D(input, uv);
}

inline float4 mixColor(float4 color, float intensity, float alpha)
{
    return float4(color.rgb + (color.rgb*intensity), alpha);
}

float4 main(float2 uv: TEXCOORD) : COLOR
{
    float4 outColor = 0;

    const float
        size = DdxDdy.w / (LAYERS),
        darkDistance = (PLACE_LIGHT_ON_BOTTOM ? size : -size) * Size;
    
    const float4
        inColor = tex2D(input, uv);

    float4 midColor = getPixel(uv, darkDistance);
    
    outColor += mixColor(midColor, GlowIntensity, inColor.a);
    outColor += mixColor(inColor, -ShadowIntensity, inColor.a);

    return (outColor + inColor) / MixDivisor;
}