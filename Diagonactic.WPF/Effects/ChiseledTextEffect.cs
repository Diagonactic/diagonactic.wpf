﻿/*
 * Copyright 2015 Matthew S. Dippel
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace Diagonactic.WPF.Effects
{
    public class ChiseledTextEffect : ShaderEffect
    {

        /// <summary>
        ///     Initializes a new instance of the <see cref="ChiseledTextEffect" /> class.
        /// </summary>
        public ChiseledTextEffect()
        {
            var pixelShader = new PixelShader();
            Uri resourceUri = Global.MakePackUri("effects/chiseledtexteffect.ps");
            pixelShader.UriSource = resourceUri;
            this.PixelShader = pixelShader;
            this.DdxUvDdyUvRegisterIndex = 9;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(SizeProperty);
            this.UpdateShaderValue(ShadowIntensityProperty);
            this.UpdateShaderValue(GlowIntensityProperty);
            this.UpdateShaderValue(MixDivisorProperty);
        }

        /// <summary>
        ///     Property for the input brush (do not use, this is set automatically to the texture of the element the effect is
        ///     being blended with)
        /// </summary>
        /// <value>
        ///     Do not set this value manually.
        /// </value>
        public Brush Input { get { return ((Brush)(this.GetValue(InputProperty))); } set { this.SetValue(InputProperty, value); } }


        #region Static Dependency Properties

        /// <summary>
        ///     The Implicit Input Property
        /// </summary>
        public static readonly DependencyProperty InputProperty =
            ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof (ChiseledTextEffect), 0);

        /// <summary>
        ///     The Glow/Shadow size property
        /// </summary>
        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register("Size", typeof (double), typeof (ChiseledTextEffect),
            new UIPropertyMetadata(0.7D, ShaderEffect.PixelShaderConstantCallback(1)));

        /// <summary>
        ///     The shadow intensity property
        /// </summary>
        public static readonly DependencyProperty ShadowIntensityProperty = DependencyProperty.Register("ShadowIntensity", typeof (double), typeof (ChiseledTextEffect),
            new UIPropertyMetadata(1D, ShaderEffect.PixelShaderConstantCallback(2)));

        /// <summary>
        ///     The glow intensity property
        /// </summary>
        public static readonly DependencyProperty GlowIntensityProperty = DependencyProperty.Register("GlowIntensity", typeof (double), typeof (ChiseledTextEffect),
            new UIPropertyMetadata(1.25D, ShaderEffect.PixelShaderConstantCallback(3)));

        /// <summary>
        ///     The mix divisor property
        /// </summary>
        public static readonly DependencyProperty MixDivisorProperty = DependencyProperty.Register("MixDivisor", typeof (double), typeof (ChiseledTextEffect),
            new UIPropertyMetadata(3.5D, ShaderEffect.PixelShaderConstantCallback(4)));

        #endregion

        #region Effect Properties

        /// <summary>Adjust the size of the Shadow/Glow portion of the effect</summary>
        /// <remarks>The glow portion will appear more pronounced than shadow as this value is adjusted.</remarks>
        public double Size
        {
            get { return ((double) (this.GetValue(SizeProperty))); }
            set
            {
                this.PaddingTop = Math.Abs(value/2);
                this.PaddingBottom = Math.Abs(value/2);
                this.SetValue(SizeProperty, value);
            }
        }

        /// <summary>Intensity of the Shadow layer</summary>
        public double ShadowIntensity { get { return ((double)(this.GetValue(ShadowIntensityProperty))); } set { this.SetValue(ShadowIntensityProperty, value); } }

        /// <summary>Intensity of the Glow layer</summary>
        public double GlowIntensity { get { return ((double)(this.GetValue(GlowIntensityProperty))); } set { this.SetValue(GlowIntensityProperty, value); } }

        /// <summary>Intensity of the blending of the effect with the background</summary>
        /// <remarks>
        ///     <para>At the end of the HLSL, the output value is divided by this value.</para>
        ///     <para>A value of 2 will result in no blending with the background.</para>
        ///     <para>Values lower than 2 will result in an increase in the intensity of the color.</para>
        ///     <para>
        ///         Values above 2 will result in a slight transparency being applied which will result in the effect blending
        ///         slightly with the background color.  3 is a sane
        ///         value for most medium-light background colors.
        ///     </para>
        /// </remarks>
        public double MixDivisor { get { return ((double)(this.GetValue(MixDivisorProperty))); } set { this.SetValue(MixDivisorProperty, value); } }

        #endregion
    }
}